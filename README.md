# API_development

To run the application first create a virtual environment and install the required dependencies for the project mentioned in the requrirements.txt file

``` Bash
python -m venv <virtual_env_name>
python -m pip install -r requirements.txt
```