from typing import Optional
from fastapi import FastAPI, status, Response, HTTPException
from fastapi.params import Body
from pydantic import BaseModel
from random import randrange
import mysql
from mysql.connector import errorcode

demoapp = FastAPI()

class Post(BaseModel):
    title: str
    content: str
    published: bool = True
    rating: Optional[int] = None

try:
    cnx = mysql.connector.connect(user='root', password='root', host='127.0.0.1', database='fastapi')
except mysql.connector.Error as err:
    print(err)
else:
    print("Connected to database")
    cnx.close()


def findPostById(id: int):
    for post in my_posts:
        if post['id'] == id:
            return post
        
def findPostIndex(id: int):
    for index,post in enumerate(my_posts):
        if post["id"] == id:
            return index

my_posts = [
    {'title': 'title1', 'content': 'content1', 'id': 1},
    {'title': 'title2', 'content': 'content2', 'id': 2},
]

@demoapp.get("/")
def root():
    return {"message": "Welcome to fast api"}

@demoapp.post("/posts", status_code=status.HTTP_201_CREATED)
def createPost(post: Post):
    id = randrange(0, 10000000)
    post_dict = post.model_dump()
    post_dict['id'] = id
    my_posts.append(post_dict)
    return {'post_created': post_dict}

@demoapp.get("/posts")
def getPosts():
    return {"posts": my_posts}

@demoapp.get("/posts/{id}")
def getPostById(id: int, response: Response):
    post = findPostById(id)

    if not post:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": f"Post with id = {id} not found"}
    
    return {'post_details': post}

@demoapp.put("/posts/{id}")
def updatePost(id: int, post: Post):
    index = findPostIndex(id)
    if not index:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Post with id = {id} not found")
    incoming_post_dict = post.model_dump()
    incoming_post_dict['id'] = id
    my_posts[index] = incoming_post_dict
    return {'updated_post': my_posts[index]}

@demoapp.delete("/posts/{id}", status_code=status.HTTP_204_NO_CONTENT)
def deletePostById(id: int):
    index = findPostIndex(id)
    if not index:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Post with id = {id} not found")
    my_posts.pop(index)
    